#ifndef PIECES_HPP
#define PIECES_HPP

#include <string>
#include <stdio.h>

#include "Piece.hpp"

class GPM;
class Player : public Piece{
    public: Player();
    ~Player();
    void usePower(GPM*);
};

class Monstre : public Piece{
    public: Monstre();
    ~Monstre();
    void setAleaXY(int);
    void move(char);
    void strategieA(char);
};

class Mur : public Piece{
    public: Mur();
    ~Mur();
};

class Chargeur : public Piece{
    public: Chargeur();
    ~Chargeur();
};

class Porte : public Piece{
    private:
        bool open;
    public: Porte();
    public: Porte(bool);
    void toggle();
    bool isOpen();
    ~Porte();
};

class Diams : public Piece{
    public: Diams();
    ~Diams();
};

class Vide : public Piece{
    public: Vide();
    ~Vide();
};

#endif