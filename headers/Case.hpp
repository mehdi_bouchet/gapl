#ifndef CASE_HPP
#define CASE_HPP

class Piece;
class Vide;
class Mur;
class Player;
class Plateau;
class Case {
    protected:
        Piece *piece;
        Plateau *plateau;
        int x, y;

    public:
        Case();
        Case(int xpos, int ypos);
        Case(Piece &p, int xpos, int ypos);
        ~Case();

        int getX();
        int getY();

        int getType();
        bool isType(int);
        Piece* getPiece();
        void putPiece(Piece* p);
        void setPlateau(Plateau*);
        Plateau* getPlateau();

        void setX(int xpos);
        void setY(int ypos);
        
};

#endif