#ifndef SYS_HPP
#define SYS_HPP

#define BOARD_DIR "./boards/"
#define GAMES_DIR "./games/"

// #define KEY_UP 72
// #define KEY_DOWN 80
// #define KEY_LEFT 75
// #define KEY_RIGHT 77

namespace SYS{
  void clearScreen();
  bool createDir(std::string const pth);
  std::string getExtension(std::string file);
  int _getch(void);
};

#endif