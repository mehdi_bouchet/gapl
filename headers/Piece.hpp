#ifndef PIECE_HPP
#define PIECE_HPP

#include <string>
#include <stdio.h>

#include "Case.hpp"

enum ELT{ 
    JOUEUR='J', 
    MUR='X', 
    MONSTRE='S',
    DIAMANT='$',
    PORTE_O='+',
    PORTE_F='-',
    CHARGEUR='*' ,
    VIDE=' '
};

class Piece {
    protected:
        int type;
        std::string description;
        bool ingame= false;
        Case* piece_c;
        int position= -1;
        
    public:
        int getX();
        int getY();
        int getType();
        const Case* getCase();
        int getPosition();
        void setPosition(int);
        bool isType(int);
        bool isIngame();
        bool isPosition(int, int);
        void setCase(Case* c);
        friend bool operator==(Piece&, Piece&);
        virtual ~Piece() = 0;
};
#endif