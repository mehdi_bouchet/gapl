#ifndef JEU_HPP
#define JEU_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Plateau.hpp"

using namespace std;

class Plateau;
class GPM;
class Jeu {
    private:
        vector<Plateau> plateaux;
        string jeu_name;

    public:
        Jeu();
        Jeu(string);
        Jeu(string, vector<Plateau>);

        string getName();
        vector<Plateau> *getPlateaux();
        Plateau* getPlateau();
        Plateau* getPlateau(int);

        int getSize();
        void addPlateau(Plateau);
        void show();

        friend Jeu* GPM::importJeuFromTXT(const std::string);
        
};
#endif