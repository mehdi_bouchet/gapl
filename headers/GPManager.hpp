#ifndef GPM_HPP
#define GPM_HPP

#include <iostream>
#include <string>

#include "Pieces.hpp"

enum _MODE{
    EXIT=-1,
    INIT=0,
    MODE_JEU=1,
    MODE_PLATEAU=2,
};
/*
    gc fichier.board c’est l’éditeur de niveau. 
    gc .game .board... permet de créer un jeu constitué des board déjà
    existants.
*/
class Plateau;
class Jeu;

class GPM{
    private:
        Plateau* plateau;
        Jeu* jeu;
        int mode;
        int curX, curY;
        int nbPouvoir, score, nbChargeur;
        
    public:
        GPM();
        ~GPM();
        void _init();
        void setMode(Plateau*);
        void setMode(Jeu*);
        void setMode(int);
        void setScore(int);

        int getScore();
        void triggerAction(int);

        Plateau* importPlateauFromTXT(const std::string);
        
        Jeu* importJeuFromTXT(const std::string);

        void showGame();
        void showPlateau();
        void showRules(Plateau*);

        void play(Plateau*);
        friend void Player::usePower(GPM*);
};

#endif