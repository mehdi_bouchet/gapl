#ifndef PLATEAU_HPP
#define PLATEAU_HPP

#define MAX_HEIGHT 50
#define MAX_WIDTH 30

#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "Piece.hpp"
#include "Pieces.hpp"
#include "GPManager.hpp"

class Case;
class GPM;
class Plateau{
    private:
        int width, height;
        int _id;

        std::string plateau_name;

        Case **board;

        Player player;
        std::vector<Mur*> murs;
        std::vector<Porte*> portes;
        std::vector<Monstre*> monstres;
        std::vector<Chargeur*> chargeurs;
        std::vector<Diams*> diamants;

    public:
        Plateau(int, int);
        Plateau(const Plateau&);
        Plateau(int, int, std::string);
        Plateau(int, int, std::string, int);
        ~Plateau();
        
        void init_plateau();
        static void preview(int, int);
        void show();

        bool is_in_board(int, int);
        bool is_side_board(int, int);
        bool is_player_position(int, int);
        bool is_free(int, int);
        bool is_blocked(int, int);

        void putPiece(Piece*, int, int);
        void putPiece(int, int, int);
        void removePiece(int, int);

        /*void removePiece(Piece*);
        void removeMur(Mur);
        void removePorte(Porte);*/
        void setPlayerPosition(int, int);

        
        void playerGoUp();
        void playerGoDown();
        void playerGoLeft();
        void playerGoRight();

       void openDoors();

        int get_width();
        int get_height();
        std::string get_name();
        std::vector<Monstre*>* get_monstres();
        std::vector<Mur*>* get_murs();
        
        int get_id();
        int get_playerY();
        int get_playerX();

        void set_name(std::string);

        friend Plateau* GPM::importPlateauFromTXT(const std::string);
        friend bool operator==(Plateau, Plateau);
        friend void Player::usePower(GPM*);
        friend void Case::putPiece(Piece*);
        // friend ostream& operator<<(ostream&, Plateau);
};

#endif