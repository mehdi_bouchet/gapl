#include "Case.hpp"
#include "Piece.hpp"
#include "Pieces.hpp"
#include "Plateau.hpp"

Case::Case(): x(1), y(1){ piece= new Vide(); piece->setCase(this); }
Case::Case(int xpos, int ypos): x(xpos), y(ypos){ piece= new Vide();  piece->setCase(this); }
Case::Case(Piece &p, int xpos, int ypos): x(xpos), y(ypos){ piece= &p; piece->setCase(this); }
Case::~Case(){ 
    //delete piece; 
    piece= nullptr; }

int Case::getX(){ return x; }
int Case::getY(){ return y; }

int Case::getType(){ return piece->getType(); }
bool Case::isType(int x){ return piece->isType(x); }

Piece* Case::getPiece(){ return piece; }
void Case::putPiece(Piece* p){
    int pos= piece->getPosition();
    if(pos != -1){
        switch(piece->getType()){
            case ELT::MUR: plateau->murs.erase(plateau->murs.begin()+pos); break;
            case ELT::MONSTRE: plateau->monstres.erase(plateau->monstres.begin()+pos); break;
            case ELT::DIAMANT: plateau->diamants.erase(plateau->diamants.begin()+pos); break;
            case ELT::PORTE_O: plateau->portes.erase(plateau->portes.begin()+pos); break;
            case ELT::PORTE_F: plateau->portes.erase(plateau->portes.begin()+pos); break;
            case ELT::CHARGEUR: plateau->chargeurs.erase(plateau->chargeurs.begin()+pos); break;
        };
    }

    if(!piece->isType(ELT::JOUEUR))
        delete piece;
    piece= p; 
    piece->setCase(this); 
}

void Case::setPlateau(Plateau *p){
    plateau= p;
}
Plateau* Case::getPlateau(){
    return plateau;
}

void Case::setX(int xpos){ x= xpos; }
void Case::setY(int ypos){ y= ypos; }
        