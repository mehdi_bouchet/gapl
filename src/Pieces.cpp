#include "Pieces.hpp"
#include "Plateau.hpp"
#include "GPManager.hpp"
#include <vector>
#include <cmath>

using namespace std;

Player::Player(){ type= ELT::JOUEUR; description= "Oueurj"; }
Player::~Player(){ piece_c= nullptr; }
void Player::usePower(GPM *gpm){
    if(gpm->nbPouvoir == 0)
        return;
    Plateau *p= piece_c->getPlateau();
    vector<Monstre *> *monstres= p->get_monstres();
    Monstre *m;
    gpm->score-= 3;
    gpm->nbPouvoir--;
    for(int i=0; i< monstres->size(); i++ ){
        m= monstres->at(i);
        m->setAleaXY(5);            //générer une position aléatoire a une distance de +5
    }
}

Monstre::Monstre(){ type= ELT::MONSTRE; description= "Streumons"; piece_c= nullptr; }
Monstre::~Monstre(){ piece_c= nullptr; }
void Monstre::setAleaXY(int minDist){
    int x,y;
    int dx, dy;
    int dist;
    int w= piece_c->getPlateau()->get_width();
    int h= piece_c->getPlateau()->get_height();
    int xJ= piece_c->getPlateau()->get_playerX();
    int yJ= piece_c->getPlateau()->get_playerY();

    do{
        x= rand()%w + 1;
        y= rand()%h + 1;
        dx = xJ - x;
        dy = yJ - y;
        dist= floor(sqrt(dx * dx + dy * dy));
    }while(dist < minDist && !piece_c->getPlateau()->is_free(x, y));
    

}
void Monstre::move(char toucheUtilisateur){
    strategieA(toucheUtilisateur);
}

void Monstre::strategieA(char toucheUtilisateur){
    int streumonsX= this->getX(); int streumonsY= this->getY();
    Plateau *p= piece_c->getPlateau();

    int xJ= p->get_playerX();
    int yJ= p->get_playerY();
    int w= p->get_width();
    int h= p->get_height();

    vector<Mur *> *remus= p->get_murs();
    Mur *m;
    int remuY, remuX;

    if(toucheUtilisateur == 'z' && streumonsX > xJ && streumonsX > 1) //PERMET DE FAIRE BOUGER LE MONSTRE 1
    {
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX - 1 == remuX && streumonsY == remuY){
                streumonsX++;
                break;
            }
        }
        streumonsX-- ;
    }
    else if(toucheUtilisateur == 'z' && streumonsX < xJ && streumonsX < w - 1){
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX + 1 == remuX && streumonsY == remuY){
                streumonsX--;
                break;
            }
        }
        streumonsX++;
    }

    
    if(toucheUtilisateur == 's' && streumonsX < xJ && xJ < w)
    {
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX + 1 == remuX && streumonsY == remuY){
                streumonsX--;
                break;
            }
        }
        streumonsX++ ;
    }
    else if(toucheUtilisateur == 's' && streumonsX > xJ && xJ > 1){
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX - 1 == remuX && streumonsY == remuY){
                streumonsX++;
                break;
            }
        }
        streumonsX--;
    }

    if(toucheUtilisateur == 'q' && streumonsY > yJ && streumonsY > 0)
    {
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX == remuX && streumonsY - 1 == remuY){
                streumonsY++;
                break;
            }
        }
        streumonsY-- ;
    }
    else if(toucheUtilisateur == 'q' && streumonsY < yJ && streumonsY < h - 1){
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX == remuX && streumonsY + 1 == remuY){
                streumonsY--;
                break;
            }
        }
        streumonsY++ ;
    }

    if(toucheUtilisateur == 'd' && streumonsY < yJ && yJ < h){
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX == remuX && streumonsY + 1 == remuY){
                streumonsY--;
                break;
            }
        }
        streumonsY++ ;
    }
    else if(toucheUtilisateur == 'd' && streumonsY > yJ && yJ > 0){
        for(int i=0; i< remus->size(); i++ ){
            m= remus->at(i);
            remuX= m->getX();
            remuY= m->getY();
            if(streumonsX == remuX && streumonsY - 1 == remuY){
                streumonsY++;
                break;
            }
        }
        streumonsY--;
    }
}
Mur::Mur(){ type= ELT::MUR; description= "Reumus"; piece_c= nullptr; }
Mur::~Mur(){ piece_c= nullptr; }

Diams::Diams(){ type= ELT::DIAMANT; description= "Diams"; piece_c= nullptr; }
Diams::~Diams(){ piece_c= nullptr; }

Chargeur::Chargeur(){ type= ELT::CHARGEUR; description= "Ceurchars"; piece_c= nullptr; }
Chargeur::~Chargeur(){ piece_c= nullptr; }

Porte::Porte(){ open= false; type= ELT::PORTE_F; description= "Reupors Fermée"; piece_c= nullptr; }
Porte::Porte(bool isOpen){ 
    open= isOpen; 
    type= (isOpen ? ELT::PORTE_O : ELT::PORTE_F); 
    description= (isOpen ? "Reupors Ouverte" : "Reupors Fermée"); 
    piece_c= nullptr; 
}
Porte::~Porte(){ piece_c= nullptr; }
void Porte::toggle(){ 
    if(open){
        open= false; type= ELT::PORTE_F; description= "Reupors Fermee";}
    else{
        open= true; type= ELT::PORTE_O; description= "Reupors Ouverte"; }
}
bool Porte::isOpen(){ return open; }

Vide::Vide(){ type= ELT::VIDE; description= "Vide"; piece_c= nullptr; }
Vide::~Vide(){ piece_c= nullptr; }