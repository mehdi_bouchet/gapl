 #include <sys/stat.h>
 #include <string>
 #include "Sys.hpp"

using namespace std;


 #ifdef _WIN32
    #include <windows.h>

    void SYS::clearScreen(){
    HANDLE                     hStdOut;
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    DWORD                      count;
    DWORD                      cellCount;
    COORD                      homeCoords = { 0, 0 };

    hStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
    if (hStdOut == INVALID_HANDLE_VALUE) return;

    /* Get the number of cells in the current buffer */
    if (!GetConsoleScreenBufferInfo( hStdOut, &csbi )) return;
    cellCount = csbi.dwSize.X *csbi.dwSize.Y;

    /* Fill the entire buffer with spaces */
    if (!FillConsoleOutputCharacter(
      hStdOut,
      (TCHAR) ' ',
      cellCount,
      homeCoords,
      &count
      )) return;

    /* Fill the entire buffer with the current colors and attributes */
    if (!FillConsoleOutputAttribute(
      hStdOut,
      csbi.wAttributes,
      cellCount,
      homeCoords,
      &count
      )) return;

    /* Move the cursor home */
    SetConsoleCursorPosition( hStdOut, homeCoords );
    }
    int SYS::getch(void) {
      return getch(void);
    }
  #else
      #include <unistd.h>
      #include <term.h>
      #include <stdlib.h>
      #include <sys/ioctl.h>
      #include <termios.h>
      #include <ncurses.h>

      void SYS::clearScreen()
      {
      if (!cur_term)
          {
          int result;
          setupterm( NULL, STDOUT_FILENO, &result );
          if (result <= 0) return;
          }
      putp( tigetstr( "clear") );
      }

      // int SYS::getch(void) {
      //   system("/bin/stty raw");
      //   int c = getchar();
      //   system("/bin/stty cooked");
      //   return c;
      // }
      int SYS::_getch(void) {
        int ch;
        keypad(stdscr, TRUE);
        nodelay(stdscr, TRUE);
        cbreak();
        for (;;) {
              if ((ch = getch()) == ERR) {
                  /* user hasn't responded
                  ...
                  */
              }
              else { 
                return ch;
              }
        }
      }
#endif

bool SYS::createDir(string const pth){
    return (mkdir(pth.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != -1 );
  }
string SYS::getExtension(string file){
    return file.substr(file.find_last_of(".") + 1);
}
