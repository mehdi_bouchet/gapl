#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cctype>
#include <ctime>
#include <ncurses.h>

#include "GPManager.hpp"
#include "Plateau.hpp"
#include "Jeu.hpp"
#include "Sys.hpp"

using namespace std;

GPM::GPM(){
    mode= _MODE::INIT;
    srand(time(NULL));
    //initscr(); //init ncurses
    SYS::createDir(BOARD_DIR);
    SYS::createDir(GAMES_DIR);

    plateau= nullptr;
    jeu= nullptr;
    nbPouvoir= score= nbChargeur= -1;
    curX= curY= -1;
}

GPM::~GPM(){
    mode= _MODE::EXIT;

    delete plateau;
    delete jeu;
    
    plateau= nullptr;
    jeu= nullptr;
    endwin(); // close ncurses
}
/*
    But: Change le mode en mode Plateau
    Sortie : Vide
*/

void GPM::setMode(Plateau* p){
    plateau= p;
    mode= _MODE::MODE_PLATEAU;
}

/*
    But: Change le mode en mode Jeu
    Sortie : Vide
*/

void GPM::setMode(Jeu* j){
    jeu= j;
    mode= _MODE::MODE_JEU;
}

/*
    But: Change le mode en mode Jeu
    Sortie : Vide
*/

void GPM::setMode(int m){
    switch(m){
        case _MODE::MODE_JEU: mode= _MODE::MODE_JEU; break;
        case _MODE::MODE_PLATEAU: mode= _MODE::MODE_PLATEAU; break;
    }
}

/*
    But: s est le nouveau score du plateau
    Sortie : Aucune
*/
void GPM::setScore(int s){
    score= s;
}

/*
    Sortie : Retourne le score
*/
int GPM::getScore(){
    return score;
}

/*
    But: Import du Plateau au format TXT
    Sortie : Le plateau du ficher name s'il existe. Sinon un plateau vide 10x10;
*/
Plateau* GPM::importPlateauFromTXT(const string name){
    string path= BOARD_DIR;
    Plateau *p;
    string pName;


    path.append(name+".board");
    ifstream pstream(path.c_str());

    if(!pstream)
        return (p= nullptr);

    int w, h, pId;
    int x=0, y=0;
    bool pass= false;
    char c;

    getline(pstream, pName);
    pstream >> pId;
    pstream >> w;
    pstream >> h;
    
    p= new Plateau(w, h, pName, pId);
    pstream.ignore(256,'\n');
    while( pstream.get(c) ){
        if( c == '\n'){
            y++;
            x= 0;
            pass= false;
            continue;
        }
        if(pass){
            pass= false;
            continue;
        }
        if(c == ELT::JOUEUR && curX == -1){
            curX= x; curY= y; score= 0;
            nbChargeur= 0;
            nbPouvoir= 0;
        }

        p->putPiece(c, x, y);
        pass= true;
        x++;        
    }
    return p;
}

/*
    But: Import du Plateau au format TXT depuis le dossier GAME_DIR/games/
    Sortie : Le plateau du ficher name s'il existe. Sinon un plateau vide 10x10;
*/
Jeu* GPM::importJeuFromTXT(const string name){
    string path= GAMES_DIR;
    string pathImportPlateau= "../"+path;
    

    path.append(name+"/play.game");
    ifstream jstream(path.c_str());

    if(!jstream)
        return nullptr;
    
    Jeu *j;
    string jName, pTriggerName;
    int numP;

    jstream >> numP;
    jstream.ignore(256,'\n');
    getline(jstream, jName);

    j= new Jeu(jName);
    Plateau *p;
    pathImportPlateau.append(jName+"/");

    for(int i=0; i<numP; i++){
        getline(jstream, pTriggerName);
        p= importPlateauFromTXT(pTriggerName);
        j->addPlateau(*p);
    }
    return j;
}
void GPM::showRules(Plateau *p){
    cout << "\t\t*******\t\t" << p->get_name() << "\t\t*******\t\t" << endl << endl;
    cout << "\tJ : ton personnage ; $ : la cible ; S : les menances" << endl;
    cout << endl;

    if( score <= 1 )
        cout << "\t\tTon score est de : " << score << " point." << endl;
    else
        cout << "\t\t\tTon score est de : " << score << " points." << endl;
    cout << endl;

    if( nbPouvoir <= 1 )
        cout << "\t\tIl te reste " << nbPouvoir << " pouvoir." << endl;
    else
        cout << "\t\tIl te reste " << nbPouvoir << " pouvoirs." << endl;
    cout << endl;

    if( nbChargeur <= 1 )
        cout << "\t\tIl reste : " << nbChargeur << " tentative de teleportation." << endl;
    else
        cout << "\t\tIl reste : " << nbChargeur << " tentatives de teleportation." << endl;
    cout << endl; cout << endl;
}
void GPM::play(Plateau *p){
    SYS::clearScreen();
    showRules(p);
    p->show();
}

void GPM::showGame(){
    SYS::clearScreen();
    jeu->show();
}

