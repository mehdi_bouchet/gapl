#include <iostream>
#include <string>

#include "Plateau.hpp"
#include "Jeu.hpp"
#include "GPManager.hpp"
#include "Sys.hpp"

using namespace std;


int main(int argc, char* argv[]){
    //   cout << "\x1b[40m green text on black background \x1b[0m \n" << endl;

    SYS::clearScreen();
    cout << "*******  BIENVENUE DANS LE JEU DE JEU GaPl  *******" << endl << endl;
    GPM _gpm;
    switch(argc){
        case 2:{
            string pName= string(argv[1]);
            Plateau *p= _gpm.importPlateauFromTXT(pName);
            _gpm.setMode(p);
            p->show();
            _gpm.play(p);
            break;
        }
        default:{
            cout << "Usage: gc NomPlateau | NomJeu pour jouer à un plateau" << endl;
            break;
        }
    }
    return 0;
}